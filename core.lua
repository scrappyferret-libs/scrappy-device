--- Required libraries.

-- Localised functions.
local getInfo = system.getInfo
local sub = string.sub
local open = io.open
local close = io.close
local pathForFile = system.pathForFile

-- Localised values

-- Static values.
local Platform = {}
Platform.OSX = "macos"
Platform.Windows = "win32"
Platform.Linux = "linux"
Platform.Android = "android"
Platform.IOS = "ios"
Platform.AppleTV = "tvos"
Platform.WindowsPhone = "winphone"
Platform.Web = "html5"
Platform.Switch = "nx64"

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

end

--- Gets the simple tag for each platform.
-- @return The name of the platform. Either 'macos', 'win32', 'android', 'ios', 'tvos', 'winphone', or 'html5'.
function library:getPlatform()
	return getInfo( "platform" )
end

--- Gets the environment name.
-- @return The name of the environment. Either 'device' or 'simulator'.
function library:getEnvironment()
    return getInfo( "environment" )
end

--- Gets the architecture.
-- @return The architecture.
function library:getArchitecture()
    return getInfo( "architectureInfo" )
end

--- Gets the model name.
-- @return The name of the model.
function library:getModel()
    return getInfo( "model" )
end

--- Checks if the game is currently running on a physical device.
-- @return True if it is, false otherwise.
function library:isReal()
    return self:getEnvironment() == "device"
end

--- Checks if the game is currently running in the simulator.
-- @return True if it is, false otherwise.
function library:isSimulator()
    return self:getEnvironment() == "simulator"
end

--- Checks if the game is currently running on a TV based console.
-- @return True if it is, false otherwise.
function library:isTV()
    if self:isAppleTV() or self:isAmazonTV() or self:isAndroidTV() then
        return true
    end
end

--- Checks if the game is currently running on an AppleTV.
-- @return True if it is, false otherwise.
function library:isAppleTV()
    return self:getPlatform() == Platform.AppleTV
end

--- Checks if the game is currently running on an Amazon FireTV.
-- @return True if it is, false otherwise.
function library:isAmazonTV()
    return self:getModel() == "AFTB"
end

--- Checks if the game is currently running on an Android TV.
-- @return True if it is, false otherwise.
function library:isAndroidTV()

	-- Is this the first time we've checked
	if self._isAndroidTV == nil then

		-- Set it to false by default
		self._isAndroidTV = false

		-- Work out of this is an AndroidTV device
		if self._launchArguments then

			local categories = self._launchArguments[ "androidIntent" ][ "categories" ]
			for i = 1, #categories, 1 do
				if categories[ i ] == "android.intent.category.LEANBACK_LAUNCHER" then
					self._isAndroidTV = true
					break
				end
			end

		end

	end

    return self:isReal() and self._isAndroidTV

end

--- Checks if the game is currently running on an iOS device.
-- @return True if it is, false otherwise.
function library:isIOS()
    return self:getPlatform() == Platform.IOS
end

--- Checks if the game is currently running on an Android device.
-- @return True if it is, false otherwise.
function library:isAndroid()
    return string.lower( self:getPlatform() ) == Platform.Android
end

--- Checks if the game is currently running on an OSX machine.
-- @return True if it is, false otherwise.
function library:isOSX()
    return self:getPlatform() == Platform.OSX
end

--- Checks if the game is currently running on a Windows machine.
-- @return True if it is, false otherwise.
function library:isWindows()
    return self:getPlatform() == Platform.Windows
end

--- Checks if the game is currently running on a Linux machine.
-- @return True if it is, false otherwise.
function library:isLinux()
    return self:getPlatform() == Platform.Linux
end

--- Checks if the game is currently running on an Amazon Kindle device.
-- @return True if it is, false otherwise.
function library:isKindle()
	return ( self:getModel() == "Kindle Fire" or self:getModel() == "WFJWI" or sub( self:getModel(), 1, 2 ) == "KF" )
end

--- Checks if the game is currently running on a desktop.
-- @return True if it is, false otherwise.
function library:isDesktop()
    return self:getModel() == "Desktop" or self:isOSX() or self:isWindows() or self:isLinux()
end

--- Checks if the game is currently running on a mobile device.
-- @return True if it is, false otherwise.
function library:isMobile()
	return self:isIOS() or self:isAndroid()
end

--- Checks if the game is currently running on an iPad.
-- @return True if it is, false otherwise.
function library:isIPad()
	return self:getModel() == "iPad"
end

--- Checks if the game is currently running on a Nintendo Switch.
-- @return True if it is, false otherwise.
function library:isNintendoSwitch()
	return self:getPlatform() == Platform.Switch
end

--- Checks if the game is currently running on a Console.
-- @return True if it is, false otherwise.
function library:isConsole()
	return self:isNintendoSwitch()
end

--- Checks if the game is currently running from the Steam store.
-- @return True if it is, false otherwise.
function library:isSteam()

	-- Get the dir seperator for this platform
	local dirSeperator = package.config:sub( 1, 1 )

	-- Get the root path
	local path = pathForFile()

	--- Removes the last directory from a path.
	-- @param path The current path.
	-- @return The new path.
	local moveUpOneDir = function( path )

		-- Remove everything after the last forward or back slash so and return the result
		return path:gsub( "(.*)%" .. dirSeperator .. ".*$", "%1" )

	end

	-- Are we on OSX?
	if self:isOSX() then

		-- Remove 'Corona' from the path
		path = moveUpOneDir( path )

		-- Remove 'Resources' from the path
		path = moveUpOneDir( path )

		-- Append the Plugins folder to the path
		path = path .. dirSeperator .. "Plugins"

		-- Append the steam api dyliib file to the end of the path
		path = path .. dirSeperator .. "libsteam_api.dylib"

	-- Are we on Windows?
	elseif self:isWindows() then

		-- Remove 'Resources' from the path
		path = moveUpOneDir( path )

		-- Append the steam api dll file to the end of the path
		path = path .. dirSeperator .. "steam_api.dll"

	-- Are we on Linux? Is anybody?
	elseif self:isLinux() then

	-- Otherwise we're on a non-desktop device
	else

		-- So just quit early and return false
		return false

	end

	-- Try to open the file
	local file = open( path, "r" )

	-- Flag to state whether the file exists or not
	local exists = false

	-- Does it exist?
	if file then

		-- Then flag it
		exists = true

		-- And close out the file handler
		close( file )

		-- And nil it
		file = nil

	end

	-- And return the result
	return exists

end
--- Checks if the game is currently in a browser.
-- @return True if it is, false otherwise.
function library:isWeb()
	return self:getPlatform() == Platform.Web or self:getEnvironment() == "browser"
end

--- Gets the name of the store that the app was built for.
-- @return The name of the store.
function library:getTargetStore()
	return getInfo( "targetAppStore" )
end

--- Gets the Android API level.
-- @return The api level.
function library:getAPILevel()
	return getInfo( "androidApiLevel" )
end

--- Checks if the hardware architecture matches OSX.
-- @return True if it does, false otherwise.
function library:architectureIsOSX()
	local arch = self:getArchitecture()
	return arch == "i386" or arch == "x86_64" or arch == "ppc" or arch == "ppc64"
end

--- Checks if the hardware architecture matches Windows.
-- @return True if it does, false otherwise.
function library:architectureIsWindows()
	local arch = self:getArchitecture()
	return arch == "x86" or arch == "x64" or arch == "IA64" or arch == "ARM"
end

--- Checks if the current GPU is known to be problematic with display.capture calls.
-- @return True if it is, false otherwise.
function library:willGPUHaveIssuesWithCaptures()

	-- All the naughty GPUs
	local gpus =
	{
		"Radeon RX560",
		"Intel UHD Graphics 630",
		"Intel(R) UHD Graphics 630",
		"Nvidia GeForce GTX 750 Ti",
		"Nvidia GeForce GTX 965m",
		"Nvidia GeForce GTX 1060",
		"Nvidia GeForce GTX 1650",
		"Nvidia GeForce GTX 1650 with Max-Q Design",
		"Nvidia GeForce GTX 2060",
		"Nvidia GeForce RTX 2070 SUPER",
		"GeForce RTX 2080 Ti/PCIe/SSE2"
	}

	-- Get the renderer name
	local renderer = system.getInfo( "GL_RENDERER" )

	-- Loop through the GPUs
	for i = 1, #gpus, 1 do

		-- Does it match the current renderer?
		if gpus[ i ] == renderer then

			-- Return true as it's a naughty one
			return true

		end
		
	end

end


--- Destroys this library.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Device library
if not Scrappy.Device then

	-- Then store the library out
	Scrappy.Device = library

end

-- Return the new library
return library
